//
//  PDFGen.swift
//  Document Scanner
//
//  Created by Md Zahidul Islam Mazumder on 9/5/20.
//  Copyright © 2020 Md Zahidul Islam Mazumder. All rights reserved.
//

import Foundation
import UIKit

class PdfGenerator{
    
    func createPDF(images: [UIImage]) {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        for image in images {
            let imgView = UIImageView.init(image: image)
            UIGraphicsBeginPDFPageWithInfo(imgView.bounds, nil)
            let context = UIGraphicsGetCurrentContext()
            imgView.layer.render(in: context!)
        }
        UIGraphicsEndPDFContext()
        //try saving in doc dir to confirm:
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        let path = dir?.appendingPathComponent("file.pdf")

        do {
            try pdfData.write(to: path!, options: NSData.WritingOptions.atomic)
        } catch {
            print("error catched")
        }

        let documentViewer = UIDocumentInteractionController(url: path!)
        documentViewer.name = "Vitul"
        //documentViewer.delegate = self
        documentViewer.presentPreview(animated: true)

    }

    
}
