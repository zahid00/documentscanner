//
//  ShowImageViewController.swift
//  Document Scanner
//
//  Created by Md Zahidul Islam Mazumder on 15/5/20.
//  Copyright © 2020 Md Zahidul Islam Mazumder. All rights reserved.
//

import UIKit
import PDFGenerator
import GoogleMobileAds

class ShowImageViewController: UIViewController, GADBannerViewDelegate {

    var bannerView: GADBannerView!
    
    var picOrderSerial = 0
    var documentsUrls = [URL]()
    var imageTitle = ""
    
    
    
    @IBOutlet weak var imgView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //addBannerViewToView()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        documentsUrls = Utilities.getDocuments()
        imgView.image = UIImage(contentsOfFile: documentsUrls[picOrderSerial].path)
        self.title = imageTitle
    }
    
    
    @IBAction func imgBtnAction(_ sender: UIBarButtonItem) {
        exportButtonAction()
    }
    
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            if let firstViewController = self.navigationController?.viewControllers.first {
                self.navigationController?.popToViewController(firstViewController, animated: true)
            }
        }))
        DispatchQueue.main.async {
            self.present(ac, animated: true)
        }
    }
    
    func shareDocument(documentPath: String) {
        if FileManager.default.fileExists(atPath: documentPath){
            let fileURL = URL(fileURLWithPath: documentPath)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [fileURL], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            }
            
            present(activityViewController, animated: true, completion: nil)
        }
        else {
            print("Document not found")
        }
    }

    func exportButtonAction() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Share", style: .default, handler: { action in
//            self.dismiss(animated: true) {
//            }
            DispatchQueue.main.async {
                self.shareDocument(documentPath: self.documentsUrls[self.picOrderSerial].path)
            }
        }))
        

        actionSheet.addAction(UIAlertAction(title: "Generate PDF", style: .default, handler: { action in
            var documentTitle = self.documentsUrls[self.picOrderSerial].path.components(separatedBy: "Documents/")[1]
            documentTitle = String(Array(documentTitle)[0..<(documentTitle.count-4)])
            let imagePath = self.documentsUrls[self.picOrderSerial].path
            self.generatePDF(imagePath: imagePath, pdfName: documentTitle)
            self.dismiss(animated: true) {
            }
        }))

        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { action in
            do {
                let filePath = self.documentsUrls[self.picOrderSerial]
                try FileManager.default.removeItem(at: filePath)
                
                if let firstViewController = self.navigationController?.viewControllers.first {
                    self.navigationController?.popToViewController(firstViewController, animated: true)
                }
            } catch {
                print("Delete error")
            }
        }))

        actionSheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem

        self.present(actionSheet, animated: true, completion: nil)
        
        //present(actionSheet, animated: true)
    }
    
    func generatePDF(imagePath: String, pdfName: String) {
        let page1 = PDFPage.imagePath(imagePath)
        let pages = [page1]
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("\(pdfName).pdf")
        
        do {
            try PDFGenerator.generate(pages, to: docURL, dpi: .default)
            showAlertWith(title: "Saved!", message: "Your image has been saved as PDF.")
        } catch (let e) {
            showAlertWith(title: "Save error", message: "Error saving as PDF.")
            print(e)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func addBannerViewToView() {
         bannerView = GADBannerView(adSize: kGADAdSizeBanner)
         bannerView.adUnitID = "ca-app-pub-4570742217514707/7829584755"
         bannerView.rootViewController = self
         bannerView.delegate = self
         bannerView.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(bannerView)
         view.addConstraints(
             [NSLayoutConstraint(item: bannerView!,
                                 attribute: .bottom,
                                 relatedBy: .equal,
                                 toItem: view,
                                 attribute: .bottomMargin,
                                 multiplier: 1,
                                 constant: 0),
              NSLayoutConstraint(item: bannerView!,
                                 attribute: .centerX,
                                 relatedBy: .equal,
                                 toItem: view,
                                 attribute: .centerX,
                                 multiplier: 1,
                                 constant: 0)
             ])
         bannerView.load(GADRequest())
     }
    
    
    
}
