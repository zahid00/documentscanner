//
//  ShowPDFViewController.swift
//  Document Scanner
//
//  Created by Md Zahidul Islam Mazumder on 15/5/20.
//  Copyright © 2020 Md Zahidul Islam Mazumder. All rights reserved.
//

import UIKit
import PDFKit

class ShowPDFViewController: UIViewController {
    
    @IBOutlet weak var pdfView: PDFView!
    
    var documentsUrls = [URL]()
    
    var pdfSerialNumber = 0
    var pdfTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let button1 = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.exportButtonTapped))
//        self.navigationItem.rightBarButtonItem = button1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        documentsUrls = Utilities.getDocuments()
        print(documentsUrls[pdfSerialNumber])
        setPDF(pdfURL: documentsUrls[pdfSerialNumber])
        //setPDF(pdfURL: URL(string: loc)!)
        self.title = pdfTitle
    }
    
    
    @IBAction func imgBtnAction(_ sender: UIBarButtonItem) {
        
        exportButtonAction()
        
    }
    
    
    
    func setPDF(pdfURL: URL) {
        if let pdfDocument = PDFDocument(url: pdfURL) {
            pdfView.displayMode = .singlePageContinuous
            pdfView.displayDirection = .vertical
            pdfView.document = pdfDocument
            pdfView.maxScaleFactor = 3.1
            pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit
            if let page = pdfDocument.page(at: 0) {
                let pageBounds = page.bounds(for: pdfView.displayBox)
                pdfView.scaleFactor = (pdfView.bounds.width) / pageBounds.width
            }
        }
    }
    
    func shareDocument(documentPath: String) {
        if FileManager.default.fileExists(atPath: documentPath){
            let fileURL = URL(fileURLWithPath: documentPath)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [fileURL], applicationActivities: nil)
            
            
            activityViewController.popoverPresentationController?.sourceView=self.view
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            }
            
            present(activityViewController, animated: true, completion: nil)
        }
        else {
            print("document not found")
        }
    }
    
    func exportButtonAction() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Share", style: .default, handler: { action in
            
            DispatchQueue.main.async {
                self.shareDocument(documentPath: self.documentsUrls[self.pdfSerialNumber].path)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { action in
            do {
                let filePath = self.documentsUrls[self.pdfSerialNumber]
                try FileManager.default.removeItem(at: filePath)
                
                if let firstViewController = self.navigationController?.viewControllers.first {
                    self.navigationController?.popToViewController(firstViewController, animated: true)
                }
            } catch {
                print("Delete error")
            }
        }))
        
        actionSheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem

        self.present(actionSheet, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
