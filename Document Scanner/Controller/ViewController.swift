//
//  ViewController.swift
//  Document Scanner
//
//  Created by Md Zahidul Islam Mazumder on 20/4/20.
//  Copyright © 2020 Md Zahidul Islam Mazumder. All rights reserved.
//

import UIKit
import WeScan
import PDFKit
import PDFGenerator
import GoogleMobileAds

class ViewController: UIViewController, UIGestureRecognizerDelegate, GADBannerViewDelegate, GADRewardedAdDelegate {
    
    var bannerView: GADBannerView!
    var rewardedAd: GADRewardedAd?
    
    var documentOrderNumber = 0
    var documents = [URL]()
    var scannedImage: UIImage!
    
    var selectedUrl = ""
    var selectImg = false
    var cell:ImageCollectionViewCell?
    var selectedIndex = [Int]()
    
    @IBOutlet weak var scanButton: UIButton!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var selectImageBtn: UIBarButtonItem!
    
    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //addBannerViewToView() 

        
        cancelBtn.title = ""
        cancelBtn.isEnabled = false
        scanButton.layer.cornerRadius = 10
        collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        documents = Utilities.getDocuments()
        collectionView.reloadData()
    }
    
    func adLoadFirstTime(){
        rewardedAd?.load(GADRequest()) { error in
          if let error = error {
            // Handle ad failed to load case.
            print(error)
          } else {
            // Ad successfully loaded.
            if self.rewardedAd?.isReady == true {
                self.rewardedAd?.present(fromRootViewController: self, delegate:self)
            }
            print("ad load")
          }
        }
    }
    
    
       /// Tells the delegate that the user earned a reward.
       func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
         print("Reward received with currency: \(reward.type), amount \(reward.amount).")
           
           
           
           
       }
       /// Tells the delegate that the rewarded ad was presented.
       func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
         print("Rewarded ad presented.")
       }
       /// Tells the delegate that the rewarded ad was dismissed.
       func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
         print("Rewarded ad dismissed.")
       }
       /// Tells the delegate that the rewarded ad failed to present.
       func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
         print("Rewarded ad failed to present.")
       }
       
    
    func addBannerViewToView() {
         bannerView = GADBannerView(adSize: kGADAdSizeBanner)
         bannerView.adUnitID = "ca-app-pub-4570742217514707/7829584755"
         bannerView.rootViewController = self
         bannerView.delegate = self
         bannerView.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(bannerView)
         view.addConstraints(
             [NSLayoutConstraint(item: bannerView!,
                                 attribute: .bottom,
                                 relatedBy: .equal,
                                 toItem: view,
                                 attribute: .bottomMargin,
                                 multiplier: 1,
                                 constant: -100),
              NSLayoutConstraint(item: bannerView!,
                                 attribute: .centerX,
                                 relatedBy: .equal,
                                 toItem: view,
                                 attribute: .centerX,
                                 multiplier: 1,
                                 constant: 0)
             ])
         bannerView.load(GADRequest())
     }
    
    
    
    @IBAction func selectImageAction(_ sender: UIBarButtonItem) {
        
        cancelBtn.title = "Cancel"
        cancelBtn.isEnabled = true
        
        selectImg = true
        
        //scanButton.setTitle("\u{21B1} PDF", for: .normal)
        //let image = UIImage(systemName: "square.and.arrow.up.fill")
        //scanButton.setBackgroundImage(image, for: .normal)
        scanButton.setImage(UIImage(named: "pdf"), for: .normal)
        scanButton.tintColor = UIColor.white
        
        self.selectImageBtn.title = "\(self.selectedIndex.count) images"
        
        
    }
    
    
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        
        cancelBtn.title = ""
        cancelBtn.isEnabled = false
        
        selectImg = false
        
        //scanButton.setTitle("Scan", for: .normal)
        self.selectImageBtn.title = "Select Images"
        //scanButton.setBackgroundImage(UIImage(systemName: "camera.fill"), for: .normal)
        scanButton.setImage(UIImage(named: "camera1"), for: .normal)
        
        
        selectedIndex = [Int]()
        collectionView.reloadData()
        
    }
    
    
    
    
    
    @IBAction func scanBtnAction(_ sender: UIButton) {
        

        
        if selectImg == false{
            
            self.scanImage()
            
        }
        else{
            
            if selectedIndex.count > 1 {
                
                // ca-app-pub-3940256099942544/1712485313
                // ca-app-pub-4570742217514707/5338673857
                rewardedAd = GADRewardedAd(adUnitID: "ca-app-pub-4570742217514707/5338673857")
                
                showSaveDialogForPDF()
                selectImg = false
                
                collectionView.reloadData()
                
            }
            
            
            
            
        }
        
        //selectedIndex = [Int]()
    }
   
    
    @IBAction func imageFromPhoneAction(_ sender: UIButton) {
        selectImage()
    }
    
    
    
    func generatePDF( pdfName: String) {
        
        //var pages = [Any]()
        
        var images = [UIImage]()
        
        if selectedIndex.count > 1{
            
            for i in selectedIndex{
                images.append(UIImage(contentsOfFile: documents[i].path) ?? UIImage())
            }
            
            //createPDF(images: images)
            //collectionView.reloadData()
        }
    
        
        //let page1 = PDFPage.imagePath(imagePath)
        //let pages = [page1]
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("\(pdfName).pdf")
        
        do {
            //try PDFGenerator.generate(pages, to: docURL, dpi: .dpi_300)
            try PDFGenerator.generate(images, to: docURL, dpi: .dpi_300)
            showAlertWith(title: "Saved!", message: "Your image has been saved as PDF.")
        } catch (let e) {
            showAlertWith(title: "Save error", message: "Error saving as PDF.")
            print(e)
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            if let firstViewController = self.navigationController?.viewControllers.first {
                self.navigationController?.popToViewController(firstViewController, animated: true)
            }
        }))
        DispatchQueue.main.async {
            self.present(ac, animated: true)
        }
    }
    
    func commonAttribiute(name:String){
        self.generatePDF(pdfName: "\(name).pdf")
        //self.scanButton.setBackgroundImage(UIImage(systemName: "camera"), for: .normal)
        self.scanButton.setImage(UIImage(named: "camera1"), for: .normal)
        //adLoadFirstTime()
        self.selectedIndex = [Int]()
        self.cancelBtn.title = ""
        self.cancelBtn.isEnabled = false
        self.selectImageBtn.title = "Select Images"
    }
    
    func showSaveDialogForPDF() {
        let now = Utilities.getTime()
        let alertController = UIAlertController(title: "Save Documents", message: "Enter document name", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Save", style: .default) { (_) in
            let name = alertController.textFields?[0].text
            if name != "" {
                if Utilities.checkSameName(fileName: name!, documents: self.documents) {
                    //self.savePicture(picture: scannedImage, imageName: "\(name!) (\(+1).jpg")
                    self.commonAttribiute(name: name!)
                } else {
                    //self.savePicture(picture: scannedImage, imageName: "\(name!).jpg")
                    self.commonAttribiute(name: name!)
                }
            } else {
                //self.savePicture(picture: scannedImage, imageName: "\(now).jpg")
                self.commonAttribiute(name: now)
            }
            self.documents = Utilities.getDocuments()
            self.collectionView.reloadData()
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
            self.selectImg = true
            //self.scanButton.setImage(UIImage(named: "camera1"), for: .normal)
            print("Cancelled",self.selectedIndex)
            
            
            
        }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "\(now)"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - Actions
    func scanImage() {
        let scannerViewController = ImageScannerController(delegate: self)
        scannerViewController.modalPresentationStyle = .fullScreen
        
        if #available(iOS 13.0, *) {
            scannerViewController.navigationBar.tintColor = .label
        } else {
            scannerViewController.navigationBar.tintColor = .black
        }
        
        present(scannerViewController, animated: true)
    }
    
    func selectImage() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    
    func savePicture(picture: UIImage, imageName: String) {
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        let data = picture.jpegData(compressionQuality: 0.9)
        FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
    }
    
    
    func showSaveDialog(scannedImage: UIImage) {
        let now = Utilities.getTime()
        let alertController = UIAlertController(title: "Save Documents", message: "Enter document name", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Save", style: .default) { (_) in
            let name = alertController.textFields?[0].text
            if name != "" {
                if Utilities.checkSameName(fileName: name!, documents: self.documents) {
                    self.savePicture(picture: scannedImage, imageName: "\(name!) (\(+1).jpg")
                } else {
                    self.savePicture(picture: scannedImage, imageName: "\(name!).jpg")
                }
            } else {
                self.savePicture(picture: scannedImage, imageName: "\(now).jpg")
            }
            self.documents = Utilities.getDocuments()
            self.collectionView.reloadData()
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "\(now)"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func responseOnTouch(_ sender:AnyObject){

        cell?.checkMark.isHidden = false
        print(index)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var documentTitle = documents[documentOrderNumber].path.components(separatedBy: "Documents/")[1]
        documentTitle = String(Array(documentTitle)[0..<(documentTitle.count-4)])
        
        if segue.identifier == "goToImage" {
            let imageDetailVC = segue.destination as! ShowImageViewController
            imageDetailVC.picOrderSerial = documentOrderNumber
            imageDetailVC.imageTitle = documentTitle
        } else if segue.identifier == "goToPDF" {
            let pdfDetailVC = segue.destination as! ShowPDFViewController
            //pdfDetailVC.loc = selectedUrl
            pdfDetailVC.pdfSerialNumber = documentOrderNumber
            pdfDetailVC.pdfTitle = documentTitle
        }
    }
    
    
}


extension ViewController:UIDocumentInteractionControllerDelegate{
    
    func createPDF1(images: [UIImage]) {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        for image in images {
            let imgView = UIImageView.init(image: image)
            UIGraphicsBeginPDFPageWithInfo(imgView.bounds, nil)
            let context = UIGraphicsGetCurrentContext()
            imgView.layer.render(in: context!)
        }
        UIGraphicsEndPDFContext()
        //try saving in doc dir to confirm:
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        let path = dir?.appendingPathComponent("file.pdf")

        do {
            try pdfData.write(to: path!, options: NSData.WritingOptions.atomic)
        } catch {
            print("error catched")
        }

        let documentViewer = UIDocumentInteractionController(url: path!)
        documentViewer.name = "Virtual"
        documentViewer.delegate = self
        documentViewer.presentPreview(animated: true)

    }
    
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.responseOnTouch(_:)))
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as? ImageCollectionViewCell
        let documentPath = documents[indexPath.row].path
        let documentExtensition = documentPath.suffix(3)
        let title = documentPath.components(separatedBy: "Documents/")[1]
        cell?.checkMark.isHidden = true
        cell?.imgTitle.text = String(Array(title)[0..<(title.count-4)])
        cell?.layer.cornerRadius = 8
        cell?.layer.borderColor = UIColor.black.cgColor
        cell?.layer.borderWidth = 0.25
        
        if documentExtensition == "jpg" {
            cell?.scImg.image = UIImage(contentsOfFile: documentPath)
            cell?.imgType.text = "JPG"
        } else if documentExtensition == "pdf" {
            if let pdfDocument = PDFDocument(url: documents[indexPath.row]) {
                if let page1 = pdfDocument.page(at: 0) {
                    cell?.scImg.image = page1.thumbnail(of: CGSize(
                        width: cell?.scImg.frame.size.width ?? 1*4,
                        height: cell?.scImg.frame.size.height ?? 1*4), for: .trimBox)
                }
            }
            cell?.imgType.text = "PDF"
        }
        //cell?.toggleSelected()
//        index = indexPath.row
        //cell?.contentView.addGestureRecognizer(tapGestureRecognizer)
//
        
        if selectedIndex.contains(indexPath.row){

            cell?.checkMark.isHidden=false

        }else{
             cell?.checkMark.isHidden=true
        }
        
        
        return cell ?? ImageCollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let documentExtensition = documents[indexPath.row].path.suffix(3)
        //selectedUrl = documents[indexPath.row].path as String
        
        print(indexPath.row)
        
        if selectImg == true   {
            
            print(documents[indexPath.row].path as String)
            
            if documentExtensition == "jpg" {
                
                if selectedIndex.contains(indexPath.row){
                    
                    print("before",selectedIndex)
                    selectedIndex.remove(at: self.selectedIndex.firstIndex(of: indexPath.row) ?? 0)
                    print("after",selectedIndex)
                }else{
                    
                    print("before",selectedIndex)
                    self.selectedIndex.append(indexPath.row)
                    print("after",selectedIndex)
                }
                
            }
            
            
            
            
            
            
            self.selectImageBtn.title = "\(self.selectedIndex.count) images"
            collectionView.reloadData()
        }
        
        else{
            

            
            documentOrderNumber = indexPath.row
            if documentExtensition == "jpg" {
                //documentOrderNumber = indexPath.row
                //print(indexPath.row)
                performSegue(withIdentifier: "goToImage", sender: nil)
            } else if documentExtensition == "pdf" {
                //documentOrderNumber = indexPath.row
                print("764",documentExtensition)
                performSegue(withIdentifier: "goToPDF", sender: nil)
            }
            
        }
        
        
        
        
    }
    
    
    

}


extension ViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 3
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: size)
    }
}


extension ViewController: ImageScannerControllerDelegate {
    func imageScannerController(_ scanner: ImageScannerController, didFailWithError error: Error) {
        assertionFailure("Error occurred: \(error)")
    }
    
    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithResults results: ImageScannerResults) {
        
        if results.doesUserPreferEnhancedScan {
            scannedImage = results.enhancedScan?.image
        } else {
            scannedImage = results.croppedScan.image
        }
        
        scanner.dismiss(animated: true, completion: nil)
        showSaveDialog(scannedImage: scannedImage)
    }
    
    func imageScannerControllerDidCancel(_ scanner: ImageScannerController) {
        scanner.dismiss(animated: true, completion: nil)
    }
    
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage else { return }
        let scannerViewController = ImageScannerController(image: image, delegate: self)
        present(scannerViewController, animated: true)
    }
}
