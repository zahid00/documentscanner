//
//  ImageCollectionViewCell.swift
//  Document Scanner
//
//  Created by Md Zahidul Islam Mazumder on 9/5/20.
//  Copyright © 2020 Md Zahidul Islam Mazumder. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var scImg: UIImageView!
    
    @IBOutlet weak var imgTitle: UILabel!
    
    
    @IBOutlet weak var imgType: UILabel!
    
    
    @IBOutlet weak var checkMark: UIImageView!
    
    func toggleSelected ()
    {
      //If image is selected.
      if (isSelected)
      {
              //Show check mark image.
              self.checkMark.isHidden = false
      }

      else
      {
               //Hide check mark image.
              self.checkMark.isHidden = true
      }
    }
}
